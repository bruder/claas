[[ `uname` != Darwin ]] && echo "Sorry, not ready for Linux yet!" && exit 1

echo "Before installing, please read TreeTagger's LICENSE first [OK]"

read

lynx -dump http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/Tagger-Licence | $PAGER

echo "Do you agree to TreeTagger's license, i.e. you don't use TreeTagger commercially? (Yes/No/Whatever)"

read answer

case $answer in
  y|Y)      : ;;
  n|N) exit 1 ;;
   * ) exit 1 ;;
esac

mkdir -p vendor/tree-tagger/{linux,win,darwin-intel} && \
cd $_

for file in tree-tagger-MacOSX-3.2-intel.tar.gz tagger-scripts.tar.gz install-tagger.sh german-par-linux-3.2-utf8.bin.gz german-chunker-par-linux-3.1.bin.gz; do
[[ ! -e $file ]] && wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/$file
done

sh ./install-tagger.sh

# Tree Tagger makes an error here, so we care about it ourselves
wget http://www.cis.uni-muenchen.de/~schmid/tools/TreeTagger/data/german-chunker-par-linux-3.1.bin.gz
gzip -cd german-chunker-par-linux-3.1.bin.gz > lib/german-chunker.par

cd -
