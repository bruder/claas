claas
=====

CLAAS -- Computational Linguistics as a Service!

SNEAK PREVIEW
-------------

* Check out the live demo: http://claas.labs.codeedition.de/

* Check out the API: `curl --data "data=Heute ist ein schoener Tag und so..." http://claas.labs.codeedition.de/api/v1/chunker`

INSTALLATION
------------
 
* npm install
* bower install
* grunt serve

Author
------

Daniel Bruder <daniel@codeedition.de>

License
-------

-- not decided yet --
