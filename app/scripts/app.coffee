'use strict'

angular.module('claasApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute'
])
  .config ['$routeProvider', '$locationProvider', ($routeProvider, $locationProvider) ->
    $routeProvider
      .when '/',
        templateUrl: 'partials/main'
        controller: 'MainCtrl'
      .when '/tokenize',
        templateUrl: 'partials/tokenize'
        controller: 'TokenizeCtrl'
      .when '/chunker',
        templateUrl: 'partials/chunker'
        controller: 'ChunkerCtrl'
      .when '/wylie-transliterate',
        templateUrl: 'partials/wylie-transliterate'
        controller: 'WylieTransliterationCtrl'
      .when '/ngram',
        templateUrl: 'partials/ngram'
        controller: 'NGramCtrl'
      .otherwise
        redirectTo: '/'
    $locationProvider.html5Mode(true)
  ]
