'use strict'

angular.module('claasApp')
  .controller 'NGramCtrl', ['$scope', '$http', ($scope, $http) ->
    $scope.backend = 
      name: 'cpp-ngram-builder',
      link: 'https://github.com/xdbr/cpp-ngram-builder'

    $scope.form = 
      data: 'foo bar baz quux'
      size: 3

    $scope.data =
      ngrams: {}

    $scope.submit = (form) -> # not $scope.form.data from here
      console.log "in submit: #{form.data}, #{form.size}"
      $http.post('/api/v1/ngram', {size: form.size, data: form.data}).success (data) ->
        $scope.data.ngrams = data.data
  ]