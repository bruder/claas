'use strict'

angular.module('claasApp')
  .controller 'ChunkerCtrl', ['$scope', '$http', ($scope, $http) ->
    $scope.data =
        chunked: {}

    $scope.submit = () ->
      $http.post('/api/v1/chunker', {data: $scope.chunker.text}).success (data) ->
        $scope.data.chunked = data.data
  ]
