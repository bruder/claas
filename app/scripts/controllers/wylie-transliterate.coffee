'use strict'

angular.module('claasApp')
  .controller 'WylieTransliterationCtrl', ['$scope', '$http', ($scope, $http) ->
    $scope.backend = 
      name: 'App-Lingua-BO-Wylie-Transliteration-0.1.0',
      link: 'https://metacpan.org/release/App-Lingua-BO-Wylie-Transliteration'

    $scope.wylieTransliterationForm = 
      text: 'bkra shis bde legs'

    $scope.data =
      transliterated: {}

    $scope.submit = () ->
      $http.post('/api/v1/wylie-transliterate', {data: $scope.wylieTransliterationForm.text}).success (data) ->
        $scope.data.transliterated = data.data
  ]