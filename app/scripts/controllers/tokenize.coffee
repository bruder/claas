'use strict'

angular.module('claasApp')
  .controller 'TokenizeCtrl', ['$scope', '$http', ($scope, $http) ->
    $scope.data =
        tokenized: {}

    $scope.submit = () ->
      $http.post('/api/v1/tokenize', {data: $scope.tokenize.text}).success (data) ->
        $scope.data.tokenized = data.data
  ]