'use strict'

angular.module('claasApp')
  .controller 'MainCtrl', ['$scope', '$http', ($scope, $http) ->
    $scope.resources = [
      { name: 'tokenize', link: 'tokenize' },
      { name: 'chunker', link: 'chunker' },
      { name: 'Wylie Classical Tibetan Transliteration', link: 'wylie-transliterate'},
      { name: 'N-Gram generation', link: 'ngram'},
    ]
  ]
