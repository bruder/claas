'use strict';

var mongoose = require('mongoose'),
    Thing = mongoose.model('Thing'),
    async = require('async'),
    inspect = require('eyes').inspector({maxLength: 2048*8}),
    _s = require('underscore.string'),
    exec = require('child_process').exec;
    
    // nlp = require("nlp-toolkit");
    // nlp = require('nlp-toolkit').tokenizer;

exports.awesomeThings = function(req, res) {
  return Thing.find(function (err, things) {
    if (!err) {
      return res.json(things);
    } else {
      return res.send(err);
    }
  });
};

exports.tokenize = function(req, res) {
  return res.json({data: _s.words(req.body.data)});
};

exports.chunker = function(req, res) {
  inspect(req.body);
  var cmd = 
    'cat <<__HERE__ | ' + 
    '/Users/dbruder/code/node/claas' + // todo: get rid of this part
    '/vendor/tree-tagger/darwin-intel/cmd/tagger-chunker-german\n' +
    req.body.data + "\n\n__HERE__";
  inspect(cmd);
  var opts = {
    encoding: 'utf8',
    timeout: 0,
    maxBuffer: 200*1024,
    killSignal: 'SIGTERM',
    cwd: true,
    env: null
  };
  var cb = function (error, stdout, stderr) {
    res.json({data: stdout});
    if (error !== null) {
      console.log('exec error: ' + error);
    }
  };
  var child = exec(cmd, opts, cb);
  
};

exports.wylieTransliteration = function(req, res) {
  inspect(req.body);
  var cmd = 
    'cat <<__HERE__ | ' +
    'wylie-transliterate\n' +
    req.body.data + "\n\n__HERE__";
  // inspect(cmd);
  var opts = {
    encoding: 'utf8',
    timeout: 0,
    maxBuffer: 200*1024,
    killSignal: 'SIGTERM',
    cwd: true,
    env: null
  };
  var cb = function (error, stdout, stderr) {
    // console.log('stdout: ' + stdout);
    // console.log('stderr: ' + stderr);
    res.json({data: stdout});
    if (error !== null) {
      console.log('exec error: ' + error);
    }
  };
  var child = exec(cmd, opts, cb);
};

exports.ngram = function(req, res) {
  inspect(req.body);
  var cmd = 
    'cat <<__HERE__ | ' +
    '3rdparty/ngram/ngram --size ' + req.body.size + ' ' + req.body.data + "\n\n__HERE__";
  // inspect(cmd);
  var opts = {
    encoding: 'utf8',
    timeout: 0,
    maxBuffer: 200*1024,
    killSignal: 'SIGTERM',
    cwd: true,
    env: null
  };
  var cb = function (error, stdout, stderr) {
    // console.log('stdout: ' + stdout);
    // console.log('stderr: ' + stderr);
    res.json({data: stdout});
    if (error !== null) {
      console.log('exec error: ' + error);
    }
  };
  var child = exec(cmd, opts, cb);
};