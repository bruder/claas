#include <iostream>
#include <string>

#include "3rdparty/ngram/NGramBuilder.hpp"
#include "3rdparty/range/range.hpp"

// compile: g++ -std=c++11 -Wall -Wextra -pedantic -I 3rdparty ngram.cpp -o ngram
//   usage; ./ngram --size 3 foo bar baz quux
int main(int ac, char*av[]) {
    if (ac < 3) {
        std::cerr << "Usage: ./ngram --size [size] ngram1 ngram2 ..." << std::endl;
        return 1;
    }

    int size = atoi(av[2]);
    std::string line;

    for (const auto & i : util::range<int>(3, ac-1)) {
        line += av[i];
        line += ' ';
    }

    for (const auto & ngram: aux::NGramBuilder<>::build_ngrams(line, size) ) {
        std::cout << ngram << std::endl;
    }

    return 0;
}